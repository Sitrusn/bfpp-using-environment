package root.classes;

import org.springframework.stereotype.Component;
import root.classes.donors.NotDefaultBean;

@Component
@UseMock(someClassToChangeDefaultClass = NotDefaultBean.class)
public class DefaultBean {
    {
        System.out.println("Default Bean");
    }
}
