package root.classes;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import root.classes.donors.NotDefaultBean;
import java.lang.annotation.Annotation;

@Configuration
public class BeanFactoryPostProcessorIml implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("useMockOrNot");
        Object useMockValue = beanDefinition.getAttribute("usemock");

        beanDefinition = beanFactory.getBeanDefinition("defaultBean");
        String beanClassName = beanDefinition.getBeanClassName();

        if (useMockValue.equals("true")) {
            try {
                Class<?> beanClass = Class.forName(beanClassName);
                Annotation annotation = beanClass.getAnnotation(UseMock.class);
                if (annotation != null) {
                    beanDefinition.setBeanClassName(NotDefaultBean.class.getName());
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Bean
    public static BeanFactoryPostProcessor useMockOrNot(Environment environment, ConfigurableListableBeanFactory beanFactory) {
        String result = environment.getProperty("usemock");
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("useMockOrNot");
        beanDefinition.setAttribute("usemock", result);
        return anyWord -> new String();
    }

}